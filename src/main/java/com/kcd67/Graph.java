package com.kcd67;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для представления графа.
 */
class Graph {
    private List<List<Integer>> adjacentVertices = null;

    List<List<Integer>> getAdjacentVertices() {
        return adjacentVertices;
    }

    /**
     * Построить неориентированный граф.
     *
     * @param edges                 Edge Вершины.
     * @param averageVerticesWeight int Вес ребра.
     * @param verticesNumber        int Количествов вершин.
     */
    boolean createUndirectedGraph(
            List<Edge> edges,
            int averageVerticesWeight,
            int verticesNumber
    ) throws NotValidVerticesNumber, NotValidEdgesNumber {
        if (verticesNumber <= 0) {
            throw new NotValidVerticesNumber("Количество вершин не может быть отрицательным или равно нулю!");
        }
        if (edges.size() < 1) {
            throw new NotValidEdgesNumber("Список вершин не может быть пустым!");
        }
        // Матрица смежности.
        adjacentVertices = new ArrayList<>(3 * verticesNumber);
        // Заполняем начальными пустыми значениями.
        for (int i = 0; i < 3 * verticesNumber; i++) {
            adjacentVertices.add(i, new ArrayList<>());
        }
        try {
            // Добавление ребер в неориентированный граф.
            for (Edge edge : edges) {
                // Получаем исходную вершину и ребра.
                int sourceVertices = edge.getSource();
                // Получаем вершину назначения из ребра.
                int destinationVertices = edge.getDestination();
                // Получаем вес вершины.
                int verticesWeight = edge.getWeight();
                // создаем две новые вершины v + N и v + 2 * N, если вес
                // края 3x. Кроме того, разбить край (v, u) на (v, v + N),
                // (v + N, v + 2N) и (v + 2N, u) каждый имеет вес x
                if (verticesWeight == 3 * averageVerticesWeight) {
                    adjacentVertices.get(sourceVertices).add(sourceVertices + verticesNumber);
                    adjacentVertices.get(sourceVertices + verticesNumber).add(sourceVertices + 2 * verticesNumber);
                    adjacentVertices.get(sourceVertices + 2 * verticesNumber).add(destinationVertices);
                }
                // создаем одну новую вершину v + N, если вес ребра
                // это 2x. Также разделите ребро (v, u) на (v, v + N),
                // (v + N, u) каждый имеет вес x
                else if (verticesWeight == 2 * averageVerticesWeight) {
                    adjacentVertices.get(sourceVertices).add(sourceVertices + verticesNumber);
                    adjacentVertices.get(sourceVertices + verticesNumber).add(destinationVertices);
                }
                // Разбиение не требуется, если вес ребра равен 1x
                else
                    adjacentVertices.get(sourceVertices).add(destinationVertices);
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}

class NotValidVerticesNumber extends Exception {
    public NotValidVerticesNumber() {
    }

    public NotValidVerticesNumber(String message) {
        super(message);
    }
}

class NotValidEdgesNumber extends Exception {
    public NotValidEdgesNumber() {
    }

    public NotValidEdgesNumber(String message) {
        super(message);
    }
}
