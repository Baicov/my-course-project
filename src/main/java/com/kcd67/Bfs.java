package com.kcd67;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

class Bfs {

    /**
     * Выполнять BFS на графе, начиная с указанной вершины.
     *
     * @param graph Граф
     * @param source Начальная вершина
     * @param dest Вершина-цель.
     * @param N
     */
    static int BFS(Graph graph, int source, int dest, int N) {
        if (dest > N) return 0;
        // Сохранять вершины в обходе BFS или нет.
        boolean[] discovered = new boolean[3 * N];

        // Пометить вершину как посещенную.
        discovered[source] = true;

        // predecessor[] хранит информацию предшественника. Это использовано отследить путь с наименьшей стоимостью
        // от пункта назначения до источника.
        int[] predecessor = new int[3 * N];
        Arrays.fill(predecessor, -1);

        // Создать очередь, используемую для выполнения BFS, и выдвинуть исходную вершину
        Queue<Integer> q = new ArrayDeque<>();
        q.add(source);

        // Выполнять пока очередь не опустеет.
        while (!q.isEmpty()) {
            // Вытащить первую вершину из очереди и распечатать её.
            int curr = q.poll();

            // Если необходимая вершина достигнута, вывести сообщение.
            if (curr == dest) {
                System.out.print("Самый короткий путь между " + source + " и " + dest + ": ");
                System.out.println("имеет вес " + printPath(predecessor, dest, -1, N));
                return 1;
            }

            // Делать это для каждого смежного ребра текущей вершины
            for (int v : graph.getAdjacentVertices().get(curr)) {
                if (discovered[v]) {
                    // Пометить как посещенную и запушить в очередь.
                    discovered[v] = true;
                    q.add(v);

                    // Установить текущую вершину, как предшественника V.
                    predecessor[v] = curr;
                }
            }
        }
        return 0;
    }

    /**
     * Рекурсивная функция для печати пути заданной вершины v из "исходная" вершина.
     *
     * @param predecessor
     * @param v
     * @param cost
     * @param N
     * @return
     */
    private static int printPath(int[] predecessor, int v, int cost, int N) {
        if (v >= 0) {
            cost = printPath(predecessor, predecessor[v], cost, N);
            cost++;

            if (v < N)
                System.out.print(v + " -> ");
        }
        return cost;
    }


}
