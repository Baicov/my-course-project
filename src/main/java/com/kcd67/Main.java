package com.kcd67;

import java.util.*;

/**
 * Главный класс приложения.
 */
class Main {

    // Поиск кратчайшего пути с использованием алгоритма поиска в ширину.
    public static void main(String[] args) throws Exception {
        /**
         * Длина пути между вершинами.
         */
        int x = 1;

        // Список ребер графа.
        List<Edge> edges = Arrays.asList(
                new Edge(0, 1, 3 * x),
                new Edge(0, 4, 1 * x),
                new Edge(1, 2, 1 * x),
                new Edge(1, 3, 3 * x),
                new Edge(1, 4, 1 * x),
                new Edge(4, 2, 2 * x),
                new Edge(4, 3, 1 * x)
        );

        // Установить количество вершин в графе.
        final int N = 5;

        // Создание графа из ребер.
        Graph graph = new Graph();
        boolean result = false;
        try {
            result = graph.createUndirectedGraph(edges, x, N);
        } catch (NotValidVerticesNumber | NotValidEdgesNumber notValidVerticesNumber) {
            notValidVerticesNumber.printStackTrace();
        } finally {
            if (!result) {
                throw new Exception("Что-то пошло не так!");
            }
        }

        // Устаанвливаем вершину из которой необходимо начать и вершину в которую ищем путь.
        int source = 0, dest = 2;

        // Запускаем алгоритм.
        Bfs.BFS(graph, source, dest, N);
    }
}