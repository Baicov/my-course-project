package com.kcd67;

/**
 * Ребро графа
 */
class Edge {
    /**
     * Из вершины.
     */
    private int source;
    /**
     * В вершину.
     */
    private int destination;
    /**
     * Вес ребра.
     */
    private int weight;

    /**
     * Получить исходную вершину.
     * @return int
     */
    int getSource() {
        return source;
    }

    /**
     * Получить вершину назначения.
     * @return int
     */
    int getDestination() {
        return destination;
    }

    /**
     * Получить вес.
     * @return int
     */
    int getWeight() {
        return weight;
    }

    /**
     * Конструктор вершины.
     *
     * @param source int Исходная вершина.
     * @param destination int Верщина назначения.
     * @param weight int Вес вершины.
     */
    Edge(int source, int destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }
}
