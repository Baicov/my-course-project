package com.kcd67;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BfsNotFoundTest {

    @Test
    void BFS() throws NotValidEdgesNumber, NotValidVerticesNumber {
        int x = 1;
        // Список ребер графа.
        List<Edge> edges = Arrays.asList(
                new Edge(0, 1, 3 * x),
                new Edge(0, 4, 1 * x),
                new Edge(1, 2, 1 * x),
                new Edge(1, 3, 3 * x),
                new Edge(1, 4, 1 * x),
                new Edge(4, 2, 2 * x),
                new Edge(4, 3, 1 * x)
        );
        // Создание графа из ребер.
        Graph graph = new Graph();
        graph.createUndirectedGraph(edges, x, 5);
        // Устаанвливаем вершину из которой необходимо начать и вершину в которую ищем путь.
        int source = 0, dest = 10;

        // Запускаем алгоритм.
        Assertions.assertEquals(0, Bfs.BFS(graph, source, dest, 5));

    }
}