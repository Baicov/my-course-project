package com.kcd67;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EdgeTest {

    Edge edge;

    @BeforeEach
    void setUp() {
        edge = new Edge(1,2,3);
    }

    @AfterEach
    void tearDown() {
        edge = null;
    }

    @Test
    void getSource() {
        assertEquals(1, edge.getSource());
    }

    @Test
    void getDestination() {
        assertEquals(2, edge.getDestination());
    }

    @Test
    void getWeight() {
        assertEquals(3, edge.getWeight());
    }
}